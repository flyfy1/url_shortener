AdminUser.create_with(password: 'ad_is_good').find_or_create_by!(email: 'admin@example.com')

user_1 = User.create_with(password: 'ad_is_good').find_or_create_by!(email: 'user1@example.com')
user_2 = User.create_with(password: 'ad_is_good').find_or_create_by!(email: 'user2@example.com')

10.times {|i| user_1.short_urls.create_with(target: 'https://www.google.com/').find_or_create_by! key: "u1k#{i}" }
25.times {|i| user_2.short_urls.create_with(target: 'https://www.bing.com/').find_or_create_by! key: "u2k#{i}" }

