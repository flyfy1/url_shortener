class CreateVisitHistories < ActiveRecord::Migration[5.0]
  def change
    create_table :visit_histories do |t|
      t.integer :short_url_id
      t.text :headers
      t.string :ip

      t.timestamps
    end
    add_index :visit_histories, :short_url_id
  end
end
