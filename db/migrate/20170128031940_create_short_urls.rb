class CreateShortUrls < ActiveRecord::Migration[5.0]
  def change
    create_table :short_urls do |t|
      t.string :key
      t.string :target

      t.timestamps
    end
    add_index :short_urls, :key
  end
end
