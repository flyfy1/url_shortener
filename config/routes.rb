Rails.application.routes.draw do
  post 'user_token' => 'user_token#create'
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  get '/:id', to: 'api/short_urls#serve'

  namespace :api do
    resources :short_urls, only: [:index, :create, :show, :destroy] do
      get :hits, on: :member
    end
  end
end
