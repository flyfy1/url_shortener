class Api::BaseController < ApplicationController
  include Rails::Pagination
  include Knock::Authenticable

  before_action :authenticate_user, except: :serve
  skip_before_action :verify_authenticity_token

  respond_to :json

  rescue_from Exception, :with => :render_exception_error

  def render_exception_error(error)
    render json: {errors: {message: error.message} }, status: :internal_server_error
  end
end