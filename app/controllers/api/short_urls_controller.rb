class Api::ShortUrlsController < Api::BaseController
  before_action only: [:serve, :show, :destroy, :hits] do
    url_key = params[:id]
    @short_url = ShortUrl.find_by key: url_key
    render json: {errors: {key: "No URL defined for given url_key: #{url_key}"}}, status: :not_found unless @short_url
  end

  before_action only: [:show, :destroy, :hits] do
    render json: {errors: {key: "URL key #{params[:id]} belongs to other user"}}, status: :unauthorized if current_user != @short_url.user
  end

  def serve
    @short_url.visit_histories.create! ip: request.ip, headers: request.env.select {|k,_| k =~ /^HTTP_/}
    redirect_to @short_url.target, status: 301
  end

  def hits
    @visit_histories = @short_url.visit_histories.page params[:page]
    paginate @visit_histories
  end

  def index
    @short_urls = current_user.short_urls.page params[:page]
    paginate @short_urls
  end

  def show
  end

  def create
    target_url = params[:target]
    key = params[:key]

    @short_url = ShortUrl.new target: target_url, key: key, user: current_user

    @short_url.key ||= ShortUrl.generate_key
    if @short_url.save
      render :show, status: :created
    else
      render json: { errors: @short_url.errors.messages }, status: :bad_request
    end
  end

  def destroy
    @short_url.destroy!
    head :no_content
  end
end
