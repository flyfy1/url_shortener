class User < ApplicationRecord
  devise :database_authenticatable, :recoverable, :rememberable
         # :registerable, :trackable, :validatable
         # :confirmable, :lockable, :timeoutable and :omniauthable

  has_many :short_urls

  alias_method :authenticate, :valid_password?
  def self.from_token_payload payload
    self.find payload["sub"]
  end
end
