class ShortUrl < ApplicationRecord
  validates_presence_of :key, :target
  validates_uniqueness_of :key
  validates_format_of :target, :with => /\A(http|https):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?\z/ix

  has_many :visit_histories, dependent: :destroy
  belongs_to :user

  def visit_count
    visit_histories.count
  end

  def self.generate_key
    base_length = 4
    key = nil

    while key.nil? do
      generated_key = SecureRandom.urlsafe_base64 base_length

      if ShortUrl.where(key: generated_key).count == 0
        key = generated_key
      else
        base_length += 1
      end
    end

    key
  end
end
