class VisitHistory < ApplicationRecord
  belongs_to :short_url

  validates_presence_of :ip

  serialize :headers
end
