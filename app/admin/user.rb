ActiveAdmin.register User do
  permit_params :email, :password

  form do |f|
    inputs do
      input :email
      input :password
    end

    actions
  end
end
