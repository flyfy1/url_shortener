ActiveAdmin.register ShortUrl do
  permit_params :key, :target
end
