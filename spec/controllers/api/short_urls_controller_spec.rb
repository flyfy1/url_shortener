require 'rails_helper'

RSpec.describe Api::ShortUrlsController, type: :controller do
  render_views
  before { ShortUrl.create! key: 'one', target: 'http://www.google.com/' }

  context 'without validation' do
    describe '#serve' do
      context 'valid' do
        it 'records the visit history' do
          expect {
            get :serve, params: {id: 'one'}
          }.to change(VisitHistory, :count).by 1
        end

        it 'redirects to the correct target' do
          get :serve, params: {id: 'one'}
          expect(response).to redirect_to 'http://www.google.com/'
        end
      end

      context 'invalid' do
        before { get :serve, params: { id: 'undefined' } }

        it { expect(response).to have_http_status(404) }
        it { expect(JSON.parse response.body).to have_key('errors') }
      end
    end
  end

  context 'with validation' do
    let(:user) { User.create! email: 'user@example.com', password: 'password123' }
    before do
      request.headers.merge authenticated_header(user)
      request.headers['Content-Type'] = 'application/json'
      request.headers['Accept'] = 'application/json'
    end

    describe '#create' do
      context 'empty request' do
        before { post :create }

        it { expect(response).to have_http_status(400) }
        it { expect(JSON.parse(response.body)['errors']).to have_key 'target' }
      end

      context 'auto create key' do
        before { post :create, params: {target: 'http://www.google.com'} }

        it { expect(response).to have_http_status(201) }
        it { expect(JSON.parse(response.body)).to include('key', 'target')}
      end

      context 'specific key' do
        before { post :create, params: {target: 'http://www.google.com', key: 'two'} }

        it { expect(response).to have_http_status(201) }
        it { expect(JSON.parse(response.body)).to include('key' => 'two', 'target' => 'http://www.google.com')}
      end
    end

    context 'own resource' do
      before { ShortUrl.create! key: 'two', target: 'http://www.google.com/', user: user }

      describe '#destroy' do
        context "other's resources" do
          before { delete :destroy, params: {id: 'one'} }
          it { expect(response).to have_http_status(401) }
        end

        context 'valid' do
          before { delete :destroy, params: {id: 'two'} }
          it { expect(response).to have_http_status(204) }
        end
      end

      describe '#show' do
        context "other's resources" do
          before { get :show, params: {id: 'one'} }
          it { expect(response).to have_http_status(401) }
        end

        context 'valid' do
          before { get :show, params: {id: 'two'} }
          it { expect(response).to have_http_status(200) }
          it { expect(JSON.parse response.body).to include( 'key' => 'two', 'target' => 'http://www.google.com/', 'visit_count' => 0 ) }
        end
      end
    end
  end
end
