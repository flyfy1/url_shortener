FactoryGirl.define do
  factory :short_url do
    sequence :key do |n|
      "key-#{n}"
    end

    target 'http://www.google.com/'
  end
end
