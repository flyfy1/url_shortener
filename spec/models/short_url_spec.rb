require 'rails_helper'

RSpec.describe ShortUrl, type: :model do
  subject { build :short_url }

  context 'valid' do
    it {is_expected.to be_valid}

    [ 'https://zh.wikipedia.org/wiki/%E7%BA%B8%E7%89%8C%E5%B1%8B', 'http://www.google.com/',
      'http://www.pocketmath.com/white-papers-q4-2015', 'http://php.net/manual/en/tutorial.php'
    ].each do |url|
      describe url do
        before { subject.target = url }
        it {is_expected.to be_valid}
      end
    end
  end

  context 'invalid' do
    describe 'missing field' do
      %i[key target].each do |field_name|
        describe "#{field_name}" do
          before { subject.send "#{field_name}=", nil ; subject.valid? }
          it { expect(subject.errors.messages).to have_key(field_name) }
        end
      end
    end

    describe 'duplicate key' do
      url = 'https://github.com/'

      before do
        ShortUrl.create! key: 'dup', target: url + 'abc'
        subject.key = 'dup'
        subject.valid?
      end

      it { expect(subject.errors.messages).to have_key(:key) }
    end

    describe 'invalid URL' do
      [ 'something-random', 'www.google.com', 'http://google/' ].each do |url|
        describe url do
          before { subject.target = url; subject.valid? }
          it { expect(subject.errors.messages).to have_key(:target) }
        end
      end
    end
  end
end
