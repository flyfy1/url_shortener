# README

## API

The API spec has been formulated in file: `docs/swagger.yaml`

When reading it, open [swagger web editor](http://editor.swagger.io/), and paste
in the content of the file. Then one can see the API in a clearer way.

## Setup

Run the following commands:

```bash
bundle install -j4
rake db:create db:migrate db:seed
rails serve
```

The following user would be created, with the following credentials:

- user 1
  - username: `user1@example.com`
  - password: `ad_is_good`
- user 2
  - username: `user2@example.com`
  - password: `ad_is_good`

The credentails above can be used to issue API calls


Additionally, an admin user has also been created:

- username: `admin@example.com`
- password: `ad_is_good`

Login to the admin page: <http://localhost:3000/admin>, you can see [the list of
URLs created](http://localhost:3000/admin/short_urls), and the [list of
users](http://localhost:3000/admin/users) who can access the API.

## Basic Use Case

### exchange for token

The user authentication is implemented following
[RFC7519](https://tools.ietf.org/html/rfc7519) (JSON Web Token). Therefore, in
order to access an endpoint with user authentication, one have to exchange for
the corresponding token: 

``` bash
curl -X POST -H "Content-Type: application/json" -H "Cache-Control: no-cache" \
  '{"auth": {"email": "user1@example.com", "password": "ad_is_good"}}' \
  "http://localhost:3000/user_token"
```

if the request above give response like: `{"jwt": "TOKEN"}`, then in the future
request where authorization is needed, the Authorization header should be used:

```
Authorization: bearer $TOKEN
```

`!TOKEN!` would be used to denote the content in the Authorization header
(`bearer $TOKEN`).

### create a new short url

supposed we want to create a short url to <http://www.pocketmath.com/>, there
are 2 ways:

#### without shortened URL given

```bash
curl -X POST -H "Accept: application/json" -H "Authorization: !TOKEN!"  \
  -H "Content-Type: application/json" -H "Cache-Control: no-cache" \
  -d '{ "target": "http://www.pocketmath.com/" }'\
  "http://localhost:3000/api/short_urls"
```

#### with shortened URL given
```bash
curl -X POST -H "Accept: application/json" -H "Authorization: !TOKEN!"  \
  -H "Content-Type: application/json" -H "Cache-Control: no-cache" \
  -d '{ "target": "http://www.pocketmath.com/", "key": "pocket" }'\
  "http://localhost:3000/api/short_urls"
```

### visit a shortened URL

open your browser, visit: <http://localhost:3000/pocket>, it would redirect you
to the corresponding target URL.

### get list of shortened URLs this user has

```bash
curl -X GET -H "Accept: application/json" -H "Authorization: !TOKEN!" \
  -H "Content-Type: application/json" -H "Cache-Control: no-cache" \
  "http://localhost:3000/api/short_urls"
```

### get info of a particular URL

```bash
curl -X GET -H "Accept: application/json" -H "Authorization: !TOKEN!" \
  -H "Content-Type: application/json" -H "Cache-Control: no-cache" \
  "http://localhost:3000/api/short_urls/pocket"
```

### get URL hits of a particular URL

```bash
curl -X GET -H "Accept: application/json" -H "Authorization: !TOKEN!" \
  -H "Content-Type: application/json" -H "Cache-Control: no-cache" \
  "http://localhost:3000/api/short_urls/pocket/hits"
```

## Other Implementation Detail

Pagination has also been done following RFC5988, for the following endpoint:

- `/api/short_urls`
- `/api/short_urls/:shortened_url/hits`

with default page size of 20
